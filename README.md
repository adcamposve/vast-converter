# README #

This project is intended to convert from VAST 1.0 to VAST 3.0 specification, using a Proxy (hosted serveless), with AWS API Gateway and AWS Lambda. 

- The proxy is deployed and configure to point to the AD Decision Server generating VAST 1.0 and the output of the response returns the VAST 3.0 XML.