var xml2js = require('xml2js');
var js2xmlparser = require("js2xmlparser");

exports.convertVAST_1_to_3 = convertVAST_1_to_3;

function convertVAST_1_to_3(vast1_0_XML, callback){
    var parser = new xml2js.Parser();
    parser.parseString(vast1_0_XML, function (err, result) { 
        var root = result['VideoAdServingTemplate'];

        //AD.InLine
        var vast3_0_JSON = {};
        vast3_0_JSON = {
            "@": {
                'xmlns:xsi': "http://www.w3.org/2001/XMLSchema-instance",
                'xsi:noNamespaceSchemaLocation': "vast.xsd",
                'version': "3.0"
            },                                 
        };
        vast3_0_JSON.Ad = { 
            "@": {
                id: ""
            }                
        };
        vast3_0_JSON.Ad.InLine = {
            AdSystem: '',
            AdTitle: '',
            Description: '',
            Error: '',
            Impression: '',
            Creatives: {
                Creative: []
            },
            Extensions: []
        };
        vast3_0_JSON.Ad["@"].id = root.Ad[0]["$"].id+root.Ad[0].InLine[0].Video[0].AdID[0];
        vast3_0_JSON.Ad.InLine.AdSystem = root.Ad[0].InLine[0].AdSystem[0];
        vast3_0_JSON.Ad.InLine.AdTitle = root.Ad[0].InLine[0].AdTitle[0];
        vast3_0_JSON.Ad.InLine.Description = root.Ad[0].InLine[0].Description[0];
        vast3_0_JSON.Ad.InLine.Impression = root.Ad[0].InLine[0].Impression[0].URL[0]["_"];

        //AD Creative
        var creative = {
            "@": {
                id: "",
                sequence: "1"
            }   
        };
        creative.Linear = {
            Duration: '',
            TrackingEvents: {
                Tracking: []
            },
            VideoClicks: {
                ClickThrough: {
                    "@": {
                        id: "GDFP-"+root.Ad[0].InLine[0].Video[0].AdID[0]
                    },
                    "#": ""
                }
            },
            MediaFiles: {
                MediaFile: []
            }
        };

        //Obj MediaFile
        var mediaFile = {
            "@": {
                id: "GDFP-"+root.Ad[0].InLine[0].Video[0].AdID[0],
                delivery: "",
                width: "",
                height: "",
                type: "",
                bitrate: "",
                scalable: "true",
                maintainAspectRatio: "true"
            },
            "#": "" 
        };
                                
        var trackingEventsOriginal = root.Ad[0].InLine[0].TrackingEvents[0].Tracking;
        for (var x = 0; x < trackingEventsOriginal.length; x++){
            //Obj Tracking
            var tracking = {
                "@": {
                    event: trackingEventsOriginal[x]["$"].event
                },
                "#": trackingEventsOriginal[x].URL[0]["_"]
            };                
            creative.Linear.TrackingEvents.Tracking.push(tracking);
        }
        
        creative["@"].id = root.Ad[0].InLine[0].Video[0].AdID[0];
        creative.Linear.VideoClicks.ClickThrough["#"] = root.Ad[0].InLine[0].Video[0].VideoClicks[0].ClickThrough[0].URL[0]["_"];
        creative.Linear.Duration = root.Ad[0].InLine[0].Video[0].Duration[0];
                    
        mediaFile["@"].delivery = root.Ad[0].InLine[0].Video[0].MediaFiles[0].MediaFile[0]["$"].delivery;
        mediaFile["@"].bitrate = root.Ad[0].InLine[0].Video[0].MediaFiles[0].MediaFile[0]["$"].bitrate;
        mediaFile["@"].width = root.Ad[0].InLine[0].Video[0].MediaFiles[0].MediaFile[0]["$"].width;
        mediaFile["@"].height = root.Ad[0].InLine[0].Video[0].MediaFiles[0].MediaFile[0]["$"].height;
        mediaFile["@"].type = root.Ad[0].InLine[0].Video[0].MediaFiles[0].MediaFile[0]["$"].type;
        mediaFile["#"] = root.Ad[0].InLine[0].Video[0].MediaFiles[0].MediaFile[0].URL[0];

        creative.Linear.MediaFiles.MediaFile.push(mediaFile);
        vast3_0_JSON.Ad.InLine.Creatives.Creative.push(creative);

        
        callback(js2xmlparser.parse('VAST',vast3_0_JSON));
    });
}