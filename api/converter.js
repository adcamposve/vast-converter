'use strict';
const uuid = require('uuid');
const AWS = require('aws-sdk');
const querystring = require('querystring');

AWS.config.setPromisesDependency(require('bluebird'));
var vastConverter = require('../utils/vastConverter.js');


module.exports.get = (event, context, callback) => {    
    'use strict';    

    const httpTransport   = require('http');
    const httpsTransport  = require('https');
    var transport = process.env.HTTPS == "false" ? httpTransport : httpsTransport;
    var hostname = process.env.HOSTNAME;
    var port = process.env.PORT;
    var path = process.env.PATH;
    var qs = querystring.stringify(event.queryStringParameters);
  
    getVAST1_0(transport, hostname, port, path, qs, function (error, statusCode, headers, body) {
        if (error){
          console.log(error);
          const response = {
            statusCode: 400,
            body: '',
          };
          callback(null, response);
        } else { 
            console.log('Original Body response  (https)');
            console.log(body);
            var vast1_0_XML = body;
            vastConverter.convertVAST_1_to_3(vast1_0_XML, function (vast3_0_XML){
                delete headers["transfer-encoding"];
                delete headers["connection"];
                delete headers["p3p"];
                delete headers["set-cookie"];
                delete headers["access-control-allow-origin"];
                var response = {
                  statusCode: statusCode,
                  headers: headers,
                  body: vast3_0_XML
                };
                callback(null, response);
            });
        }
    }); 

};

function getVAST1_0(httpTransport, hostname, port, path, querystring, callback) {
    'use strict';
            
    const responseEncoding = 'utf8';
    const httpOptions = {
        hostname: hostname,
        port: port,
        path: path+'?'+querystring,
        method: 'GET',
        headers: {}
    };
    httpOptions.headers['User-Agent'] = 'node ' + process.version;
     
    const request = httpTransport.request(httpOptions, (res) => {
        let responseBufs = [];
        let responseStr = '';
        
        res.on('data', (chunk) => {
            if (Buffer.isBuffer(chunk)) {
                responseBufs.push(chunk);
            }
            else {
                responseStr = responseStr + chunk;            
            }
        }).on('end', () => {
            responseStr = responseBufs.length > 0 ? 
                Buffer.concat(responseBufs).toString(responseEncoding) : responseStr;
            
            callback(null, res.statusCode, res.headers, responseStr);
        });
        
    })
    .setTimeout(0)
    .on('error', (error) => {
        callback(error);
    });
    request.write("")
    request.end();
}